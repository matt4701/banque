package fr.gouv.finances.dgfip.banque.v1.entites;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Table(uniqueConstraints = {
        @UniqueConstraint(name = "UniqueNomEtPrenom", columnNames = { "nom",
                "prenom" }) })
@Entity
public class Personne {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String nom;
    private String prenom;

    @OneToMany(cascade = { CascadeType.PERSIST,
            CascadeType.REMOVE }, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "titulaire")
    private List<CompteBancaire> comptes = new ArrayList<CompteBancaire>();

    @OneToMany(cascade = { CascadeType.PERSIST,
            CascadeType.REMOVE }, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "titulaire")
    private List<CarteBancaire> cartes = new ArrayList<CarteBancaire>();

    public Personne() {
    }

    public Personne(String nom, String prenom) {
	this.nom = nom;
	this.prenom = prenom;
    }

    public String getNom() {
	return nom;
    }

    public void setNom(String nom) {
	this.nom = nom;
    }

    public String getPrenom() {
	return prenom;
    }

    public void setPrenom(String prenom) {
	this.prenom = prenom;
    }

    public List<CompteBancaire> getComptes() {
	return comptes;
    }

    public void setComptes(List<CompteBancaire> comptes) {
	this.comptes = comptes;
    }

    public List<CarteBancaire> getCartes() {
	return cartes;
    }

    public void setCartes(List<CarteBancaire> cartes) {
	this.cartes = cartes;
    }

    public UUID getId() {
	return id;
    }

    public void setId(UUID id) {
	this.id = id;
    }

    @Override
    public String toString() {
	return "Personne [nom=" + nom + ", prenom=" + prenom + "]";
    }
}