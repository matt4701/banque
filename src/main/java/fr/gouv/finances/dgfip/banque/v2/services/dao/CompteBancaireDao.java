package fr.gouv.finances.dgfip.banque.v2.services.dao;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import fr.gouv.finances.dgfip.banque.v1.entites.Banque;
import fr.gouv.finances.dgfip.banque.v1.entites.CompteBancaire;

public interface CompteBancaireDao
        extends CrudRepository<CompteBancaire, UUID> {
    Iterable<CompteBancaire> findByBanque(Banque banque);

    CompteBancaire findByCodeGuichetAndNumCompte(String codeGuichet,
            String numCompte);
}
