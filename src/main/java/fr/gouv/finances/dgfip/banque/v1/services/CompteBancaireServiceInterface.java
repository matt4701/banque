package fr.gouv.finances.dgfip.banque.v1.services;

import fr.gouv.finances.dgfip.banque.v1.CompteException;
import fr.gouv.finances.dgfip.banque.v1.entites.CompteBancaire;
import fr.gouv.finances.dgfip.banque.v1.entites.CompteEpargne;

public interface CompteBancaireServiceInterface {

    public void afficherSyntheseOperations(CompteBancaire compte);

    public void creerOperation(CompteBancaire compte, String string, double d)
            throws CompteException;

    public double calculerInteret(CompteEpargne compte);

    public void supprimer(CompteBancaire compte);
}
