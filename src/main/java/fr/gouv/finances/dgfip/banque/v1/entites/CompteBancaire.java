package fr.gouv.finances.dgfip.banque.v1.entites;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public abstract class CompteBancaire {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected UUID id;

    public UUID getId() {
	return id;
    }

    protected String codeBanque;
    protected String codeGuichet;
    protected String numCompte;
    protected String cle;
    protected Double solde;

    // @JoinColumn(name = "UUID", nullable = false, updatable = false)
    @ManyToOne
    private Banque banque;

    @ManyToOne
    protected Personne titulaire;

    @OneToMany(cascade = { CascadeType.PERSIST,
            CascadeType.REMOVE }, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "compte")
    protected List<Operation> operations = new ArrayList<Operation>();

    @ManyToMany(fetch = FetchType.EAGER // , cascade = CascadeType.ALL // {
                                        // CascadeType.PERSIST
                                        // }
    )
    protected List<CarteBancaire> cartes = new ArrayList<CarteBancaire>();

    public List<CarteBancaire> getCartes() {
	return cartes;
    }

    public void setCartes(CarteBancaire carte) {
	if (!this.cartes.contains(carte)) {
	    this.cartes.add(carte);
	}
    }

//  // bloc d'initialisation
//  {
//	  this.codeBanque = "";
//  }

    public CompteBancaire() {

    }

    public CompteBancaire(String codeBanque, String codeGuichet,
            String numCompte, String cle, Double solde) {
	this.codeBanque = codeBanque;
	this.codeGuichet = codeGuichet;
	this.numCompte = numCompte;
	this.cle = cle;
	this.solde = solde;

    }

    public String getCodeBanque() {
	return codeBanque;
    }

    public void setCodeBanque(String codeBanque) {
	this.codeBanque = codeBanque;
    }

    public String getCodeGuichet() {
	return codeGuichet;
    }

    public void setCodeGuichet(String codeGuichet) {
	this.codeGuichet = codeGuichet;
    }

    public String getNumCompte() {
	return numCompte;
    }

    public List<Operation> getOperations() {
	return operations;
    }

    public void setOperations(Operation operation) {
	this.operations.add(operation);
    }

    public Personne getTitulaire() {
	return titulaire;
    }

    public void setTitulaire(Personne titulaire) {
	this.titulaire = titulaire;
    }

    public void setNumCompte(String numCompte) {
	this.numCompte = numCompte;
    }

    public String getCle() {
	return cle;
    }

    public void setCle(String cle) {
	this.cle = cle;
    }

    public Double getSolde() {
	return solde;
    }

    public void setSolde(Double solde) {
	this.solde = solde;
    }

    public abstract Double calculerSolde();

    public String getRib() {
	return String.format("%s %s %s %s", this.codeBanque, this.codeGuichet,
	        this.numCompte, this.cle);
    }

    /********************************/

    public Banque getBanque() {
	return this.banque;
    }

    public void setBanque(Banque banque) {
	this.banque = banque;
    }

    @Override
    public String toString() {
	return "CompteBancaire [codeBanque=" + codeBanque + ", codeGuichet="
	        + codeGuichet + ", solde=" + solde + ", banque=" + banque
	        + ", titulaire=" + titulaire + "]";
    }

}
