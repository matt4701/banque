package fr.gouv.finances.dgfip.banque.v1.entites;

import java.util.UUID;

import javax.persistence.Entity;

@Entity
public class CompteEpargne extends CompteBancaire {
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private UUID id;

    public CompteEpargne() {

    }

    public CompteEpargne(UUID id, double txInteret) {
	super();
	this.id = id;
	this.txInteret = txInteret;
    }

    public CompteEpargne(String codeBanque, String codeGuichet,
            String numCompte, String cle, Double solde, double txInteret) {
	super(codeBanque, codeGuichet, numCompte, cle, solde);
	this.txInteret = txInteret;
    }

    protected double txInteret;

    @Override
    public Double calculerSolde() {
	// TODO Auto-generated method stub
	double soldeInitial = this.solde;
	for (Operation op : this.operations) {
	    soldeInitial += op.montant;
	}
	return soldeInitial;
    }

    @Override
    public String toString() {
	return "CompteEpargne [id=" + id + ", txInteret=" + txInteret
	        + ", codeBanque=" + codeBanque + ", codeGuichet=" + codeGuichet
	        + ", numCompte=" + numCompte + ", cle=" + cle + ", solde="
	        + solde + "]";
    }

}
