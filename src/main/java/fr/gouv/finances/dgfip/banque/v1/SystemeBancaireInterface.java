package fr.gouv.finances.dgfip.banque.v1;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.gouv.finances.dgfip.banque.v1.entites.Banque;
import fr.gouv.finances.dgfip.banque.v1.entites.Personne;

@Service
public interface SystemeBancaireInterface {
    List<Personne> listeAdherent(Banque banque);

    public List<String> rechercherRIBCompteCarte(Banque banque, String numCarte)
            throws SystemeBancaireException;

    public boolean verifierCodePin(Banque banque, String numCarte,
            String codePin) throws SystemeBancaireException;

    public int creerOperation(Banque banque, String ribCompte, String libelle,
            Double montant) throws SystemeBancaireException;
}
