package fr.gouv.finances.dgfip.banque.v1.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.gouv.finances.dgfip.banque.v1.SystemeBancaireException;
import fr.gouv.finances.dgfip.banque.v1.SystemeBancaireInterface;
import fr.gouv.finances.dgfip.banque.v1.entites.Banque;
import fr.gouv.finances.dgfip.banque.v1.entites.CarteBancaire;
import fr.gouv.finances.dgfip.banque.v1.entites.CompteBancaire;
import fr.gouv.finances.dgfip.banque.v1.entites.Operation;
import fr.gouv.finances.dgfip.banque.v1.entites.Personne;
import fr.gouv.finances.dgfip.banque.v1.services.CompteBancaireServiceInterface;
import fr.gouv.finances.dgfip.banque.v2.services.dao.CarteBancaireDao;
import fr.gouv.finances.dgfip.banque.v2.services.dao.CompteBancaireDao;
import fr.gouv.finances.dgfip.banque.v2.services.dao.OperationDao;

@Component
public class SystemeBancaire implements SystemeBancaireInterface {
    @Autowired
    private CompteBancaireServiceInterface compteBancaireService;
    @Autowired
    private CarteBancaireDao carteBancaireDao;
    @Autowired
    private CompteBancaireDao compteBancaireDao;

    @Autowired
    private OperationDao operationDao;

    @Override
    public List<Personne> listeAdherent(Banque banque) {
	// TODO Auto-generated method stub
	HashMap<CompteBancaire, Personne> map = banque.getMapCompteAPersonne();
	List<Personne> liste = new ArrayList<Personne>();
	for (Map.Entry mapentry : map.entrySet()) {
//	    System.out.println("clé: " + mapentry.getKey() + " | valeur: "
//	            + mapentry.getValue());
	    liste.add((Personne) mapentry.getValue());
	}
	return liste;

//	return banque.getMapCompteAPersonne().values().stream()
//	        .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public List<String> rechercherRIBCompteCarte(Banque banque, String numCarte)
            throws SystemeBancaireException {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public boolean verifierCodePin(Banque banque, String numCarte,
            String codePin) throws SystemeBancaireException {
	// TODO Auto-generated method stub
	for (CarteBancaire cb : banque.getCarte()) {
	    if (numCarte.equals(cb.getNumCarte())
	            && codePin.equals(cb.getCodePin())) {
		return true;
	    }
	}
	return false;
    }

    @Override
    public int creerOperation(Banque banque, String ribCompte, String libelle,
            Double montant) throws SystemeBancaireException {
	// TODO Auto-generated method stub
	Operation op = new Operation(libelle, montant);
	for (CompteBancaire compte : banque.getComptes()) {
	    if (compte.getRib().equals(ribCompte)) {
		op.setCompte(compte);
		compte.getOperations().add(op);
		operationDao.save(op);
		return 1;
	    }
	}
	throw new SystemeBancaireException("Opération impossible");
    }

}