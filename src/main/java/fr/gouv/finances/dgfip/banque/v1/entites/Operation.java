package fr.gouv.finances.dgfip.banque.v1.entites;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Operation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    protected Integer numOperation;
    protected Date dateOperation;
    protected String libelle;
    protected double montant;
    protected static int num = 0;

    @ManyToOne
    protected CompteBancaire compte;

    public Operation(UUID id, Integer numOperation, Date dateOperation,
            String libelle, double montant, CompteBancaire compte) {
	super();
	this.id = id;
	this.numOperation = numOperation;
	this.dateOperation = dateOperation;
	this.libelle = libelle;
	this.montant = montant;
	this.compte = compte;
    }

    public CompteBancaire getCompte() {
	return compte;
    }

    public void setCompte(CompteBancaire compte) {
	this.compte = compte;
    }

    public Operation() {

    }

    public Operation(String libelle, Double montant) {
	super();
	num += 1;
	this.numOperation = num;
	this.dateOperation = new Date();
	this.libelle = libelle;
	this.montant = montant;
    }

    public Integer getNumOperation() {
	return numOperation;
    }

    public void setNumOperation(Integer numOperation) {
	this.numOperation = numOperation;
    }

    public Date getDateOperation() {
	return dateOperation;
    }

    public void setDateOperation(Date dateOperation) {
	this.dateOperation = dateOperation;
    }

    public String getLibelle() {
	return libelle;
    }

    public void setLibelle(String libelle) {
	this.libelle = libelle;
    }

    public double getMontant() {
	return montant;
    }

    public void setMontant(double montant) {
	this.montant = montant;
    }

    public static int getNum() {
	return num;
    }

    public static void setNum(int num) {
	Operation.num = num;
    }
}
