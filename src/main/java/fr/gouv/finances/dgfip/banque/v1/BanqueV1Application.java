package fr.gouv.finances.dgfip.banque.v1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("fr.gouv.finances.dgfip.banque")
@EnableJpaRepositories("fr.gouv.finances.dgfip.banque.v2.services.dao")
public class BanqueV1Application {
    public static void main(String[] args) throws Exception {
	SpringApplication.run(BanqueV1Application.class, args);
    }
}
