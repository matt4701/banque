package fr.gouv.finances.dgfip.banque.v2.services.dao;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import fr.gouv.finances.dgfip.banque.v1.entites.Banque;

public interface BanqueDao extends CrudRepository<Banque, UUID> {

    Banque findByCodeBanque(String codeBanque);

}
