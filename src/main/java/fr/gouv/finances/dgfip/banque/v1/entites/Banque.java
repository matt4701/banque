package fr.gouv.finances.dgfip.banque.v1.entites;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import fr.gouv.finances.dgfip.banque.v1.Util;

@Entity
public class Banque {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String codeBanque;

    private HashMap<CompteBancaire, Personne> mapCompteAPersonne = new HashMap<CompteBancaire, Personne>();

    @OneToMany(cascade = { CascadeType.PERSIST,
            CascadeType.REMOVE }, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "banque")
    private List<CompteBancaire> comptes = new ArrayList<CompteBancaire>();

    @OneToMany(cascade = { CascadeType.PERSIST,
            CascadeType.REMOVE }, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "banque")
    private List<CarteBancaire> carte = new ArrayList<CarteBancaire>();

    public void setCodeBanque(String codeBanque) {
	this.codeBanque = codeBanque;
    }

    public UUID getId() {
	return id;
    }

    public void setId(UUID id) {
	this.id = id;
    }

    public void setMapCompteAPersonne(
            HashMap<CompteBancaire, Personne> mapCompteAPersonne) {
	this.mapCompteAPersonne = mapCompteAPersonne;
    }

    public void setCarte(List<CarteBancaire> carte) {
	this.carte = carte;
    }

    public List<CompteBancaire> getComptes() {
	return comptes;
    }

    public void setComptes(List<CompteBancaire> comptes) {
	this.comptes = comptes;
    }

    public List<CarteBancaire> getCarte() {
	return carte;
    }

    public HashMap<CompteBancaire, Personne> getMapCompteAPersonne() {
	return this.mapCompteAPersonne;
    }

    public Banque() {
    }

    public Banque(String codeBanque) {
	this.codeBanque = codeBanque;
    }

    public String getCodeBanque() {
	return codeBanque;
    }

    public Personne getTitulaire(CompteBancaire compte) {
//	return this.mapCompteAPersonne.get(compte);
	return compte.getTitulaire();
    }

    @Override
    public String toString() {
	return "Banque [codeBanque=" + codeBanque + "]";
    }

    public void afficherSyntheseComptes() {
	System.out.println(
	        "+-----------------+--------------------------+----------------------+------------+\n"
	                + "| Type compte     | RIB                      | Titulaire            | Solde      |\n"
	                + "+-----------------+--------------------------+----------------------+------------+");
	for (CompteBancaire compte : this.getComptes()) {
	    Personne personne = compte.getTitulaire();
	    String compteType = compte.getClass().toString().equals(
	            "class fr.gouv.finances.dgfip.banque.v1.entites.CompteEpargne")
	                    ? "Compte epargne"
	                    : "Compte courant";
	    String fullName = String.format("%s %s", personne.getNom(),
	            personne.getPrenom());
	    String paddedCompteType = Util.padRightSpaces(compteType, 15);
	    String paddedRib = compte.getRib();
	    String paddedFullName = Util.padRightSpaces(fullName, 20);
	    String paddedSolde = Util
	            .padLeftSpaces(compte.calculerSolde().toString(), 10);
	    System.out.println("| " + paddedCompteType + " | " + paddedRib
	            + " | " + paddedFullName + " | " + paddedSolde + " |");
	}
	;
	System.out.println(
	        "+-----------------+--------------------------+----------------------+------------+");
    }
}
