package fr.gouv.finances.dgfip.banque.v1.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.gouv.finances.dgfip.banque.v1.BanqueV1Application;
import fr.gouv.finances.dgfip.banque.v1.CompteException;
import fr.gouv.finances.dgfip.banque.v1.entites.CompteBancaire;
import fr.gouv.finances.dgfip.banque.v1.entites.CompteEpargne;
import fr.gouv.finances.dgfip.banque.v1.entites.Operation;
import fr.gouv.finances.dgfip.banque.v1.services.CompteBancaireServiceInterface;
import fr.gouv.finances.dgfip.banque.v2.services.dao.CompteBancaireDao;
import fr.gouv.finances.dgfip.banque.v2.services.dao.OperationDao;

@Component("solde")
public class CompteBancaireService implements CompteBancaireServiceInterface {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(BanqueV1Application.class);

    @Autowired
    private OperationDao operationDao;
    @Autowired
    private CompteBancaireDao compteBancaireDao;

    public Double calculerSolde(CompteBancaire compte) {
	double solde = 0.0;
//	for (Operation op : compte.getOperations()) {
//	    solde += op.getMontant();
//	}
	for (Operation op : operationDao.findByCompte(compte)) {
	    solde += op.getMontant();
	}
	return solde;

    }

    @Override
    public void afficherSyntheseOperations(CompteBancaire compte) {
	// TODO Auto-generated method stub
	LOGGER.info("\n Liste des opérations");
	for (Operation op : operationDao.findByCompte(compte)) {
	    LOGGER.info("opération n° : " + op.getNumOperation()
	            + " effectué le : " + op.getDateOperation() + " libelle : "
	            + op.getLibelle() + " : " + op.getMontant());
	}
	double solde = this.calculerSolde(compte);
	LOGGER.info("Votre solde est de : " + solde);
    }

    @Override
    public void creerOperation(CompteBancaire cc, String string, double d)
            throws CompteException {
	if (cc.getClass().equals(CompteEpargne.class)) {
	    if ((this.calculerSolde(cc) + d) < 0) {
		throw new CompteException(
		        "Votre compte épargne ne peut être inférieur à 0");
	    }
	}
	Operation operation = new Operation(string, d);
	operation.setCompte(cc);
	cc.getOperations().add(operation);
	operationDao.save(operation);
    }

    @Override
    public double calculerInteret(CompteEpargne compte) {
	return 0;
    }

    @Override
    public void supprimer(CompteBancaire compte) {
	compteBancaireDao.deleteById(compte.getId());
    }
}
