package fr.gouv.finances.dgfip.banque.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.gouv.finances.dgfip.banque.v1.entites.Banque;
import fr.gouv.finances.dgfip.banque.v1.entites.CarteBancaire;
import fr.gouv.finances.dgfip.banque.v1.entites.CompteBancaire;

@Component
public class Gabier {
    @Autowired
    private SystemeBancaireInterface systemeBancaire;

//    public Gabier(SystemeBancaireInterface systemeBancaire) {
//	// TODO Auto-generated constructor stub
//	this.systemeBancaire = systemeBancaire;
//    }

    public List<String> accessCompte(Banque banque, String numCarte,
            String codePin) throws SystemeBancaireException {
	if (systemeBancaire.verifierCodePin(banque, numCarte, codePin)) {
	    List<String> comptes = new ArrayList<String>();

	    for (CarteBancaire cb : banque.getCarte()) {
		if (numCarte.equals(cb.getNumCarte())) {
		    for (CompteBancaire cc : cb.getComptes()) {
			comptes.add(cc.getRib());
		    }
		}
		return comptes;
	    }

	}
	throw new SystemeBancaireException("Accès impossible");
    }

    public int retirerEspeces(Banque banque, String ribCompte, Double montant)
            throws SystemeBancaireException {
	montant = 0 - montant;
	if (systemeBancaire.creerOperation(banque, ribCompte, "retrait gabier",
	        montant) == 1) {
	    return 1;
	}
	return 0;

    }
}
