package fr.gouv.finances.dgfip.banque.v1.services.impl;

import java.util.UUID;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.dgfip.banque.v1.BanqueV1Application;
import fr.gouv.finances.dgfip.banque.v1.CompteException;
import fr.gouv.finances.dgfip.banque.v1.Util;
import fr.gouv.finances.dgfip.banque.v1.entites.Banque;
import fr.gouv.finances.dgfip.banque.v1.entites.CarteBancaire;
import fr.gouv.finances.dgfip.banque.v1.entites.CompteBancaire;
import fr.gouv.finances.dgfip.banque.v1.entites.CompteCourant;
import fr.gouv.finances.dgfip.banque.v1.entites.CompteEpargne;
import fr.gouv.finances.dgfip.banque.v1.entites.Personne;
import fr.gouv.finances.dgfip.banque.v1.services.BanqueServiceInterface;
import fr.gouv.finances.dgfip.banque.v2.services.dao.BanqueDao;
import fr.gouv.finances.dgfip.banque.v2.services.dao.CarteBancaireDao;
import fr.gouv.finances.dgfip.banque.v2.services.dao.CompteBancaireDao;
import fr.gouv.finances.dgfip.banque.v2.services.dao.PersonneDao;

@Service
public class BanqueService implements BanqueServiceInterface {
    @Autowired
    private BanqueDao banqueDao;
    @Autowired
    private CompteBancaireDao compteBancaireDao;
    @Autowired
    private CarteBancaireDao carteBancaireDao;
    @Autowired
    private PersonneDao personneDao;

    private static final Logger LOGGER = LoggerFactory
            .getLogger(BanqueV1Application.class);

    public CarteBancaire lierCarte(CarteBancaire carte, CompteBancaire compte) {
	carte.getComptes().add(compte);
	return carte;
    }

    @Override
    public CompteCourant creerCompteCourant(Banque banque, Personne titulaire,
            String codeGuichet, Double soldeInitial) throws CompteException {
	if (soldeInitial < 0) {
	    throw new CompteException("Solde initial négatif");
	}
//	Integer numCompteInt = banque.getMapCompteAPersonne().size();
	Integer numCompteInt = banque.getComptes().size();
	String numComptePadded = Util.padLeftZeros(numCompteInt.toString(), 10);
	String cle = "99";

	CompteCourant newCompteCourant = new CompteCourant(
	        banque.getCodeBanque(), codeGuichet, numComptePadded, cle,
	        soldeInitial);
	newCompteCourant.setBanque(banque);
	newCompteCourant.setTitulaire(titulaire);

	banque.getMapCompteAPersonne().put(newCompteCourant, titulaire);
	banque.getComptes().add(newCompteCourant);

	return compteBancaireDao.save(newCompteCourant);
    }

    @Override
    public CompteCourant creerCompteCourant(Banque banque, Personne titulaire,
            String guichet) throws CompteException {
//	Integer numCompteInt = banque.getComptes().size();
//	String numComptePadded = Util.padLeftZeros(numCompteInt.toString(), 10);
//	String cle = "99";
//
//	CompteCourant newCompteCourant = new CompteCourant(
//	        banque.getCodeBanque(), guichet, numComptePadded, cle, 0.0);
//	newCompteCourant.setBanque(banque);
//	newCompteCourant.setTitulaire(titulaire);
//
//	banque.getMapCompteAPersonne().put(newCompteCourant, titulaire);
//	banque.getComptes().add(newCompteCourant);
//
//	return compteBancaireDao.save(newCompteCourant);

	Integer numCompteInt = banque.getComptes().size();
	String numComptePadded = Util.padLeftZeros(numCompteInt.toString(), 10);
	String cle = "99";

	CompteCourant newCompteCourant = new CompteCourant(
	        banque.getCodeBanque(), guichet, numComptePadded, cle, 0.0);
	newCompteCourant.setBanque(banque);

	try {
	    Personne titulaireInDB = personneDao.save(titulaire);
	    newCompteCourant.setTitulaire(titulaireInDB);
	} catch (Exception e) {
	    LOGGER.error(e.getMessage());
	    throw new CompteException("Utilisateur déjà existant en DB");
	}

	banque.getMapCompteAPersonne().put(newCompteCourant, titulaire);
	banque.getComptes().add(newCompteCourant);

	return compteBancaireDao.save(newCompteCourant);

    }

    @Override
    public void afficherSyntheseComptes(Banque banque) {
	banque.afficherSyntheseComptes();
    }

    @Override
    public CompteEpargne creerCompteEpargne(Banque banque, Personne titulaire,
            String guichet, Double soldeInitial, Double taux)
            throws CompteException {
	// TODO Auto-generated method stub
	if (soldeInitial < 0) {
	    throw new CompteException("Solde initial négatif");
	}
//	Integer numCompteInt = banque.getMapCompteAPersonne().size();
	Integer numCompteInt = banque.getComptes().size();
	String numComptePadded = Util.padLeftZeros(numCompteInt.toString(), 10);
	String cle = "99";

	CompteEpargne newCompteEpargne = new CompteEpargne(
	        banque.getCodeBanque(), guichet, numComptePadded, cle,
	        soldeInitial, taux);
	newCompteEpargne.setBanque(banque);
	newCompteEpargne.setTitulaire(titulaire);

	banque.getMapCompteAPersonne().put(newCompteEpargne, titulaire);
	banque.getComptes().add(newCompteEpargne);

	return compteBancaireDao.save(newCompteEpargne);
    }

    @Override
    public CarteBancaire creerCarte(Banque banque, Personne titulaire,
            CompteCourant compte) throws CompteException {
	// TODO Auto-generated method stub
	CarteBancaire newCarte = new CarteBancaire(banque, titulaire, compte);
	banque.getCarte().add(newCarte);
	// this.lierCarte(newCarte, compte);
//	return carteBancaireDao.save(newCarte);

//	compte.setCartes(newCarte);
//	compteBancaireDao.save(compte);
//	CarteBancaire carteInDB = carteBancaireDao
//	        .findByBanqueAndNumCarte(banque, newCarte.getNumCarte());
//	return carteInDB;

	CarteBancaire carteInDB = carteBancaireDao.save(newCarte);
	compte.setCartes(carteInDB);
	compteBancaireDao.save(compte);
	return carteInDB;
    }

    @Override
    public void afficherSyntheseCartes(Banque banque) {
	// TODO Auto-generated method stub
	for (CarteBancaire carte : banque.getCarte()) {
	    LOGGER.info("Carte numéro : " + carte.getNumCarte()
	            + ", titulaire : " + carte.getTitulaire() + " , code pin : "
	            + carte.getCodePin() + "\n");
	}
    }

    @Override
    public Banque creerBanque(String nomBanque) {
	return banqueDao.save(new Banque(nomBanque));
    }

    @Override
    public CompteBancaire findCompte(String codeGuichet, String numCompte) {
	return compteBancaireDao.findByCodeGuichetAndNumCompte(codeGuichet,
	        numCompte);
    }

    @Override
    @Transactional
    public void deleteAccount(UUID id) {
	System.err.println("BanqueService::deleteAccount id: " + id);
	compteBancaireDao.deleteById(id);
    }

    @Override
    public Banque getBanque(String codeBanque) {
	return banqueDao.findByCodeBanque(codeBanque);
    }

}
