package fr.gouv.finances.dgfip.banque.v2.services.dao;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import fr.gouv.finances.dgfip.banque.v1.entites.CompteBancaire;
import fr.gouv.finances.dgfip.banque.v1.entites.Operation;

public interface OperationDao extends CrudRepository<Operation, UUID> {
    Iterable<Operation> findByCompte(CompteBancaire compte);
}
