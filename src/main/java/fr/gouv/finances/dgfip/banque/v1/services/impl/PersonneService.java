package fr.gouv.finances.dgfip.banque.v1.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.gouv.finances.dgfip.banque.v1.entites.Personne;
import fr.gouv.finances.dgfip.banque.v1.services.PersonneServiceInterface;
import fr.gouv.finances.dgfip.banque.v2.services.dao.PersonneDao;

@Component
public class PersonneService implements PersonneServiceInterface {
    @Autowired
    private PersonneDao personneDao;

    @Override
    public Personne creerPersonne(String nom, String prenom) throws Exception {
	try {
	    return personneDao.save(new Personne(nom, prenom));
	} catch (Exception e) {
	    throw new Exception("Utilisateur déjà présent en DB.");
	}
//	return personneDao.save(new Personne(nom, prenom));
    }

}
