package fr.gouv.finances.dgfip.banque.v1.entites;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class CarteBancaire {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String codePin;
    protected String numCarte;
    protected Date dateExpiration;

    /*****************************/
    @ManyToMany(mappedBy = "cartes", fetch = FetchType.EAGER)
    private List<CompteBancaire> comptes = new ArrayList<CompteBancaire>();

    @ManyToOne
    private Banque banque;

    @ManyToOne
    private Personne titulaire;

    @Transient
    protected Random random = new Random();

    /*****************************/
    public CarteBancaire() {
	this.codePin = "";
	this.numCarte = "";
	this.dateExpiration = new Date();
    }

    public String getCodePin() {
	return codePin;
    }

    public void setCodePin(String codePin) {
	this.codePin = codePin;
    }

    public String getNumCarte() {
	return numCarte;
    }

    public void setNumCarte(String numCarte) {
	this.numCarte = numCarte;
    }

    public Date getDateExpiration() {
	return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
	this.dateExpiration = dateExpiration;
    }

    public List<CompteBancaire> getComptes() {
	return comptes;
    }

    public void setCompte(CompteBancaire compte) {
	if (!this.comptes.contains(compte)) {
	    this.comptes.add(compte);
	}
    }

    public void deleteCompte(CompteBancaire compte) {
	List<CompteBancaire> l_c = comptes.stream()
	        .filter(c -> !c.getNumCompte().equals(compte.getNumCompte()))
	        .collect(Collectors.toList());

	this.comptes = l_c;
    }

    public Banque getBanque() {
	return banque;
    }

    public void setBanque(Banque banque) {
	this.banque = banque;
    }

    public CarteBancaire(Banque banque, Personne titulaire, CompteCourant cc) {
	super();
	this.banque = banque;
	this.comptes.add(cc);
	this.titulaire = titulaire;
	Calendar c = Calendar.getInstance();
	c.add(Calendar.YEAR, +3);
	this.dateExpiration = c.getTime();
	SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-YYYY");
	String date1 = format1.format(this.dateExpiration);
	this.codePin = randomCode();
	this.numCarte = randomCarte();
    }

    public Personne getTitulaire() {
	return titulaire;
    }

    public void setTitulaire(Personne titulaire) {
	this.titulaire = titulaire;
    }

    public void verifierPin(String pin) {

    }

    protected String randomCode() {
	String code = "";
	for (int i = 0; i < 4; i++) {
	    Integer n = random.nextInt(10);
	    code += String.valueOf(n);
	}
	return code;
    }

    protected String randomCarte() {
	String numero = "4990";
	for (int i = 0; i < 12; i++) {
	    Integer n = random.nextInt(10);
	    numero += String.valueOf(n);
	}
	return numero;
    }

    @Override
    public String toString() {
	return "CarteBancaire [id=" + id + ", codePin=" + codePin
	        + ", numCarte=" + numCarte + ", dateExpiration="
	        + dateExpiration + ", comptes=" + comptes + ", banque=" + banque
	        + ", titulaire=" + titulaire + "]";
    }

//    @Override
//    public String toString() {
//	return "CarteBancaire [id=" + id + ", codePin=" + codePin
//	        + ", numCarte=" + numCarte + ", dateExpiration="
//	        + dateExpiration + "]";
//    }

}
